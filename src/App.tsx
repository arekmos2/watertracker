import React from 'react';
import Root from './navigation/Root';
import 'react-native-gesture-handler';
import {AuthProvider} from './contexts/useAuth';
import {CupsProvider} from './contexts/useWaterCups';
import PushNotification from 'react-native-push-notification';

PushNotification.configure({});
export default () => (
  <AuthProvider>
    <CupsProvider>
      <Root />
    </CupsProvider>
  </AuthProvider>
);
