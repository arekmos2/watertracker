import PushNotification from 'react-native-push-notification';

export default class NotifService {
  overrideDailyWaterCupNotifications() {
    const startDate = new Date();
    startDate.setHours(8, 31, 0, 0);
    Array.from({length: 8}, (v, k) => k).forEach((i) => {
      const notifDate = new Date(startDate.getTime() + 7200000 * i);
      PushNotification.localNotificationSchedule({
        title: 'Przypomienie',
        message: 'Wypij wodę',
        date: notifDate,
        id: i,
        repeatType: 'day',
      });
    });
  }

  checkInitializedNotif() {
    PushNotification.getScheduledLocalNotifications((cb) => {
      console.log(JSON.stringify(cb, null, 1));
    });
  }
}
