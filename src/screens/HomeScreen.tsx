import React, {useEffect} from 'react';
import {
  Alert,
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Bar} from 'react-native-progress';
import WaterCup from '../components/atom/WaterCup';
import {useWaterCups} from '../contexts/useWaterCups';
import NotifService from '../services/NotifService';

const WIDTH = Dimensions.get('screen').width;

export default function HomeScreen() {
  const {allFilled, cups, fillCup, progress, cupsFilledLength} = useWaterCups();

  const onCupPress = (cup: boolean, index: number) => {
    if (!cup) {
      fillCup(index);
    }
  };

  useEffect(() => {
    new NotifService().overrideDailyWaterCupNotifications();
  }, []);

  useEffect(() => {
    if (allFilled) {
      Alert.alert('Brako Ty', 'Na dziś koniec :)');
    }
  }, [allFilled]);

  return (
    <View style={styles.root}>
      <View style={styles.barContainer}>
        <Bar
          progress={progress}
          width={WIDTH * 0.9}
          height={25}
          borderRadius={5}
          animationConfig={{bounciness: 20}}
          animationType="spring"
          useNativeDriver
        />
        <Text style={styles.barText}>{cupsFilledLength * 250}ml</Text>
      </View>

      <View style={styles.cupContainer}>
        {cups.map((cup, i) => (
          <TouchableOpacity
            key={i}
            onPress={() => onCupPress(cup, i)}
            style={styles.cup}>
            <WaterCup filled={cup} />
          </TouchableOpacity>
        ))}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    marginTop: 20,
  },
  barContainer: {
    position: 'relative',
  },
  barText: {
    position: 'absolute',
    top: 5,
    left: 0,
    right: 0,
    fontWeight: '700',
    textAlign: 'center',
  },
  header: {
    color: 'black',
  },
  cupContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  cup: {
    padding: 20,
  },
});
