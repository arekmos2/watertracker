import React, {useState} from 'react';
import {
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Spinner from '../components/atom/Spinner';
import {useAuth} from '../contexts/useAuth';

export default function AuthScreen() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const {signInLoading, emailSignIn} = useAuth();

  const onSignInPress = () => {
    setError('');
    emailSignIn(email, password, () =>
      setError('Nieprawidłowy email lub hasło.'),
    );
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.root}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <SafeAreaView style={styles.root}>
          <View style={styles.headerSection}>
            <Text style={styles.header}>Water Tracker</Text>
          </View>
          <View style={styles.formSection}>
            <TextInput
              style={styles.input}
              placeholderTextColor="gray"
              placeholder="Login"
              autoCapitalize="none"
              textContentType="emailAddress"
              onChangeText={setEmail}
              value={email}
            />
            <TextInput
              style={styles.input}
              placeholderTextColor="gray"
              placeholder="Hasło"
              secureTextEntry
              onChangeText={setPassword}
              value={password}
            />
            <TouchableOpacity
              onPress={onSignInPress}
              containerStyle={styles.buttonContent}>
              <Text>Zaloguj się</Text>
            </TouchableOpacity>
            {!!error && <Text style={styles.error}>{error}</Text>}
          </View>
          <Spinner visible={signInLoading} />
        </SafeAreaView>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: 'lightskyblue',
  },
  header: {
    color: 'black',
    textAlign: 'center',
    fontSize: 30,
  },
  headerSection: {
    flex: 1,
    justifyContent: 'center',
  },
  formSection: {
    flex: 1,
    marginHorizontal: 20,
    padding: 20,
    borderRadius: 20,
  },
  input: {
    height: 40,
    backgroundColor: 'white',
    marginVertical: 5,
    borderRadius: 10,
    padding: 10,
  },
  buttonContent: {
    marginVertical: 5,
    height: 40,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: 'yellow',
  },
  error: {
    marginTop: 5,
    textAlign: 'center',
    fontWeight: '500',
    color: 'red',
  },
});
