import {useCallback, useMemo, useState} from 'react';
import {makeContext} from '../utils/makeContext';

type CupsContextProps = {
  cups: boolean[];
  fillCup: (index: number) => void;
  progress: number;
  allFilled: boolean;
  cupsFilledLength: number;
};

const {useConsumer, Provider} = makeContext<CupsContextProps>(() => {
  const [cups, setCup] = useState(Array.from({length: 8}, (v, k) => false));

  const fillCup = useCallback(
    (index: number) =>
      setCup([
        ...cups.slice(0, index),
        true,
        ...cups.slice(index + 1, cups.length),
      ]),
    [cups],
  );

  const cupsFilledLength = cups.reduce((p, n) => +p + +n, 0);

  const progress = useMemo(() => cupsFilledLength / cups.length, [cups]);

  const allFilled = cups.every((s) => s == true);

  return {cups, fillCup, progress, allFilled, cupsFilledLength};
});

export const useWaterCups = useConsumer;
export const CupsProvider = Provider;
