import {useCallback, useEffect, useState} from 'react';
import {makeContext} from '../utils/makeContext';
import auth, {FirebaseAuthTypes} from '@react-native-firebase/auth';

type AuthContextProps = {
  initializing: boolean;
  user: FirebaseAuthTypes.User | null;
  emailSignIn: (
    email: string,
    password: string,
    onError?: () => void,
  ) => Promise<FirebaseAuthTypes.UserCredential>;
  signInLoading: boolean;
  logout: () => Promise<void>;
};

const {useConsumer, Provider} = makeContext<AuthContextProps>(() => {
  const [initializing, setInitializing] = useState(true);
  const [signInLoading, setSignInLoading] = useState(false);
  const [user, setUser] = useState<FirebaseAuthTypes.User | null>(null);

  function onAuthStateChanged(user: FirebaseAuthTypes.User | null) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  const emailSignIn = useCallback(
    async (email: string, password: string, onError?: () => void) => {
      setSignInLoading(true);
      try {
        const response = await auth().signInWithEmailAndPassword(
          email,
          password,
        );
        setSignInLoading(false);
        return response;
      } catch (e) {
        setSignInLoading(false);
        onError && onError();
        return Promise.reject();
      }
    },
    [],
  );

  const logout = useCallback(() => auth().signOut(), []);

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber;
  }, []);

  return {initializing, user, emailSignIn, logout, signInLoading};
});

export const useAuth = useConsumer;
export const AuthProvider = Provider;
