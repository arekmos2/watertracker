import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {StatusBar, StyleSheet, Text} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useAuth} from '../contexts/useAuth';
import AuthScreen from '../screens/AuthScreen';
import HomeScreen from '../screens/HomeScreen';

const Stack = createStackNavigator();

export default function Root() {
  const {user, logout} = useAuth();
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <NavigationContainer>
        <Stack.Navigator>
          {user == null ? (
            <Stack.Screen
              options={{headerShown: false}}
              name="Auth"
              component={AuthScreen}
            />
          ) : (
            <Stack.Screen
              options={{
                headerRight: () => (
                  <TouchableOpacity
                    onPress={logout}
                    containerStyle={styles.headerButton}>
                    <Text>Wyloguj</Text>
                  </TouchableOpacity>
                ),
              }}
              name="Home"
              component={HomeScreen}
            />
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
}

const styles = StyleSheet.create({
  headerButton: {
    padding: 10,
  },
});
