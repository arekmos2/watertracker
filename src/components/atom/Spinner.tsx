import React from 'react';
import {ActivityIndicator, StyleSheet, View} from 'react-native';

export default function Spinner({visible}: {visible: boolean}) {
  if (!visible) return null;

  return (
    <View style={styles.root}>
      <ActivityIndicator size="large" color="black" />
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.3)',
    justifyContent: 'center',
  },
});
