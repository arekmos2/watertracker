import * as React from 'react';
import Svg, {G, Path} from 'react-native-svg';

export default function WaterCup({filled = false}: {filled?: boolean}) {
  return (
    <Svg width={80} height={80} viewBox="0 0 18.338 17.096">
      <G data-name="Group 1">
        <Path
          data-name="Path 2"
          d="M2.124 5.996c7.007-2.218 8.458 2.242 14 0l-2 10.532h-10z"
          fill={filled ? '#5fafff' : 'none'}
        />
        <Path
          data-name="Path 1"
          d="M.624.496l3.5 16.1h10l3.59-16.1z"
          fill="none"
          stroke="#707070"
          strokeLinejoin="round"
        />
      </G>
    </Svg>
  );
}
