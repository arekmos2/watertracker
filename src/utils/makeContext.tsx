import React, {FC, createContext, useContext} from 'react';

export type Hook<T> = () => T;

type ContextType<T> = {
  withProvider: (Component: FC) => FC;
  useConsumer: Hook<T>;
  Provider: FC;
};

export const makeContext = function <T>(contextHook: Hook<T>): ContextType<T> {
  const Context = createContext<T>({} as T);

  const Provider: FC = ({...rest}) => (
    <Context.Provider value={contextHook()} {...rest} />
  );

  const withProvider = (Component: FC): FC => (): JSX.Element => (
    <Provider>
      <Component />
    </Provider>
  );

  const useConsumer = (): T => useContext(Context);

  return {
    withProvider,
    useConsumer,
    Provider,
  };
};
